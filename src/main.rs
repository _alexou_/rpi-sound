// mod service;
mod bluetooth;
mod config;
mod gpio;
mod service;
mod template;
mod web_interface;
mod websocket;
// mod test;

use colored::Colorize;
use config::Config;

#[tokio::main]
async fn main() {
    config::init_config();
    let config = config::read_config();
    print_first_time_message(config);

    println!("Service started");

    let bluetooth_handler = bluetooth::handle_bt_events();
    let web_handler = web_interface::main();

    let _join = tokio::join!(bluetooth_handler, web_handler);
}

/// ensures that the user reads about raspberry pi pins before starting the program
fn print_first_time_message(config: Config) {
    if config.first_time_launch {
        let message = r#"Warning! The GPIO pins on a raspberry pi ARE NOT numbered in order. Ex: GPIO 23 is connected to pin #16"#;
        let message2 =
            r#"Please refer to a guide that explains which ports are connected to which pins."#;
        let message3 = format!(
            r#"To stop seeing this message again, open the file {} and change {} to {}"#,
            "rpi-config.toml".bold().blue(),
            "first_time_launch".bold().blue(),
            "false".bold().on_green()
        );
        let message4 = "Please verify that all the pins are connected to the right gpio port";
        println!(
            "{}\n{message2}\n{message3}\n{}\nExiting with code 1",
            message.yellow(),
            message4.bold().red()
        );
        std::process::exit(1)
    }
}
