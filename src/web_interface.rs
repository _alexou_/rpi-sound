use actix_web::{
    get,
    http::StatusCode,
    web::{self},
    App, HttpResponse, HttpServer, Responder,
};
use local_ip_address::local_ip;

use crate::websocket;
use crate::template;

pub async fn main() -> std::io::Result<()> {
    // creates the server
    let mut server = HttpServer::new(|| {
        App::new()
            .service(index)
            .service(websocket::ws_route)
            .service(modify_settings)
            .default_service(web::route().to(not_found))
    });
    server = server.shutdown_timeout(0).disable_signals();

    // binds the ip addresses to the server
    server = server.bind(("127.0.0.1", 8080)).unwrap();
    let lan_addr = local_ip().unwrap();
    server = server.bind((lan_addr, 8080)).unwrap();
    println!("ip address: {}", lan_addr);

    server.run().await
}

/// handles any pages that aren't handled by the
async fn not_found() -> impl Responder {
    actix_web::web::Redirect::to("../").see_other()

    // HttpResponse::NotFound().body("Page not found.")
}

#[get("/")]
async fn index() -> HttpResponse {
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        // .body("123")
        .body(include_str!("../views/index.html"))
}
#[get("/preferences")]
async fn modify_settings() -> HttpResponse {
    println!("wer");
    let html = template::render_preferences();
    HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(html)
}
