use actix::prelude::*;
use actix_web::{get, web, App, Error, HttpRequest, HttpResponse, HttpServer};
use actix_web_actors::ws;
use std::time::{Duration, Instant};

// Define WebSocket actor
struct WebSocketActor {
    start_time: Instant,
}
static mut bt_devides: Vec<&'static str> = Vec::new();

impl Actor for WebSocketActor {
    type Context = ws::WebsocketContext<Self>;

    // Method called when WebSocket is connected
    fn started(&mut self, ctx: &mut Self::Context) {
        self.start_time = Instant::now(); // Record the start time
        self.send_time_since_connection(ctx); // Send initial time since connection
    }
}

// Handle incoming WebSocket messages
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WebSocketActor {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        match msg {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg), // Respond to ping messages
            _ => (),
        }
    }
}

// Handle WebSocket actor messages
impl WebSocketActor {
    // Send time since connection
    fn send_time_since_connection(&self, ctx: &mut ws::WebsocketContext<Self>) {
        // Send time since connection as a WebSocket message
        // ctx.text(format!("{}", time_since_connection));
        unsafe { ctx.text(format!("{:?}", bt_devides)) };

        // Schedule the next time update
        ctx.run_interval(Duration::from_secs(1), move |_actor, ctx| {
            // let duration_since_connection = Instant::now().duration_since(actor.start_time);
            // let time_since_connection = duration_since_connection.as_secs_f64();
            unsafe { ctx.text(format!("{:?}", bt_devides)) };
        });
    }
}
#[get("/ws")]
// WebSocket route handler
async fn ws_route(req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, Error> {
    ws::start(
        WebSocketActor {
            start_time: Instant::now(),
        },
        &req,
        stream,
    )
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::thread::spawn(tt);

    HttpServer::new(|| App::new().service(ws_route))
        .bind("127.0.0.1:8080")?
        .run()
        .await
}
fn tt() {
    loop {
        std::thread::sleep(Duration::from_secs(1));
        unsafe { bt_devides.push("as") };
        unsafe { println!("list: {:#?}", bt_devides) };
    }
}
