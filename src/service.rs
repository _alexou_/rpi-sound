
pub fn create_service() {
    let _service = format!(
        r#"[Unit]
    Description=Rust Application
    [Service]
    User={username}
    WorkingDirectory=/home/{user_name}/rpi-sound 
    ExecStart=/home/{user_name}/rpi-sound
    TimeoutStopSec=10
    Restart=on-failure
    RestartSec=5[Install]
    WantedBy=multi-user.target"#,
        username = "TT",
        user_name = "TT"
    );
}
