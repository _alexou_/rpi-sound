use crate::config;
use std::thread;
use std::time::Duration;
use rppal::gpio::Gpio;

type Error = Box<dyn std::error::Error>;

/// sends a signal to turn on the amp
pub fn power_on_amp() -> Result<(), Error> {
    let is_powered = check_amp_status()?;
    if !is_powered {
        // Retrieve the GPIO pin and configure it as an output.
        let mut pin = Gpio::new()?
            .get(config::DEFAULT_AMP_POWER_PIN)?
            .into_output();

        println!("powering on amp");

        // sends the the power signal to the amp
        // pin.toggle();
        pin.set_high();
        thread::sleep(Duration::from_millis(100));
        pin.set_low();
    };
    Ok(())
}
/// sends a signal to turn off the amp
pub fn power_off_amp() -> Result<(), Error> {
    let is_powered = check_amp_status()?;
    if is_powered {
        // Retrieve the GPIO pin and configure it as an output.
        let mut pin = Gpio::new()?
            .get(config::DEFAULT_AMP_POWER_PIN)?
            .into_output();

            println!("powering off amp");


        // sends the the power signal to the amp
        // pin.toggle();
        pin.set_high();
        thread::sleep(Duration::from_millis(100));
        pin.set_low();
    };
    Ok(())
}

pub fn check_amp_status() -> Result<bool, Error> {
    // let pin = match Gpio::new().unwrap().get(config::DEFAULT_AMP_STATUS_PIN) {
    //     Ok(e) => e.into_input(),
    //     Err(_) => std::process::exit(1)
    // };
    let pin = Gpio::new()?
        .get(config::DEFAULT_AMP_STATUS_PIN)?
        .into_input();
    Ok(pin.is_high())
}

// Gpio uses BCM pin numbering. BCM GPIO 23 is tied to physical pin 16.
// const GPIO_LED: u8 = 23;

// fn loop_signal() -> Result<(), Box<dyn Error>> {
//     // Retrieve the GPIO pin and configure it as an output.
//     let mut pin = Gpio::new()?.get(GPIO_LED)?.into_output();

//     loop {
//         pin.toggle();
//         thread::sleep(Duration::from_millis(500));
//     }
// }
