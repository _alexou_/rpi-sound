/// used to power on and off the amp
pub const DEFAULT_AMP_POWER_PIN: u8 = 23; // connected to pin #16
/// used to check if the amp is on or off
pub const DEFAULT_AMP_STATUS_PIN: u8 = 24;
use lazy_static::lazy_static;
use std::{
    fs,
    path::PathBuf,
    sync::{Arc, Mutex},
};
//connected to pin #18
use serde::{Deserialize, Serialize};

lazy_static! {
    static ref CONFIG: Arc<Mutex<Config>> = Arc::new(Mutex::new(Config::new()));
}

pub fn get_config() -> Config {
    CONFIG.lock().unwrap().clone()
}

pub fn update_config() {
    let mut config = CONFIG.lock().unwrap();
    config.first_time_launch =true;
    // Update other configuration parameters as needed
}

impl Config {
    fn new() -> Self {
        read_config()
    }
}
pub fn read_config() -> Config {
    let mut path = PathBuf::new();
    path.push("rpi-config.toml");
    if !path.is_file() {
        init_config()
    }
    let toml_str = fs::read_to_string(path).unwrap();
    toml::from_str(&toml_str).unwrap()
}

pub fn launch_first_time(mut config: Config) {
    let mut path = PathBuf::new();

    // path.push(".config");
    // path.push("rpi-sound");
    path.push("rpi-config.toml");
    config.first_time_launch = false;

    let config_str: String = toml::to_string(&config).unwrap();
    fs::write(path, config_str).unwrap();
}

/// creates the confg file
pub fn init_config() {
    let mut path = PathBuf::new();
    // path.push(".config");
    // path.push("rpi-sound");
    path.push("rpi-config.toml");
    if path.exists() {
        return;
    }

    let config = Config {
        first_time_launch: true,
        amp_power_port: 23,
        amp_status_port: 24,
        button_port: 25,
        default_bt_device: None,
        button_behaviour: ButtonBehaviour::None,
        connect_behaviour: BluetoothBehaviour::None,
    };

    let config_str: String = toml::to_string(&config).unwrap();
    fs::write(path, config_str).unwrap();
}

#[derive(Serialize, Debug, Deserialize, Clone)]
pub struct Config {
    // if this is the first time the user has launched the program. used to verify that the user knows where all of the pins are
    pub first_time_launch: bool,
    pub amp_power_port: i8,
    pub amp_status_port: i8,
    pub button_port: i8,
    pub default_bt_device: Option<String>,
    pub button_behaviour: ButtonBehaviour,
    pub connect_behaviour: BluetoothBehaviour,
}

#[derive(Serialize, Debug, Deserialize, Clone)]
// what the button does
pub enum ButtonBehaviour {
    Connect,
    None,
    ShutDown,
    DisconnectBt,
}
#[derive(Serialize, Debug, Deserialize, Clone)]
pub enum BluetoothBehaviour {
    // automatically powers on the amp on a new connection or off if the device is disconnected
    PowerSwitch,
    None,
}
