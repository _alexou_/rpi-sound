use crate::config;
use crate::gpio;


use actix_web::web::get;
use btleplug::api::Peripheral;
use btleplug::api::{Central, CentralEvent, Manager as _, ScanFilter};
use btleplug::platform::PeripheralId;
use btleplug::platform::{Adapter, Manager};
use futures::stream::StreamExt;
use std::error::Error;
use std::thread;
use std::time::Duration;

async fn get_central(manager: &Manager) -> Result<Adapter, Box<dyn Error>> {
    let adapters = manager.adapters().await?;
    println!("got bluetooth adapters");
    Ok(adapters.into_iter().next().ok_or("can't list bluetooth adapters")?)

}
pub async fn handle_bt_events()-> Result<(), Box<dyn Error>>{
    config::init_config();
    let _config = config::read_config();
    println!("Service started");

    let manager = Manager::new().await?;

    // get the first bluetooth adapter
    // connect to the adapter
    let central = get_central(&manager).await?;

    // Each adapter has an event stream, we fetch via events(),
    let mut events = central.events().await?;

    // start scanning for devices
    central.start_scan(ScanFilter::default()).await?;

    // Print based on whatever the event receiver outputs. Note that the event
    // receiver blocks, so in a real program, this should be run in its own
    // thread (not task, as this library does not yet use async channels).
    while let Some(event) = events.next().await {
        match event {
            // sends a power on signal when a deivice is connected
            CentralEvent::DeviceConnected(id) => {
                println!("DeviceConnected: {:?}", id);
                match gpio::power_on_amp(){
                    Ok(_)=> (),
                    Err(_)=> ()
                }
                println!("will disconnect...");
                let peripheral = central.peripheral(&id).await?;
                
                thread::sleep(Duration::from_millis(1000));
                let tt = peripheral.disconnect().await?;
                

            }
            // sends a power off signal when a device is disconnected
            CentralEvent::DeviceDisconnected(id) => {
                println!("DeviceDisconnected: {:?}", id);
                // gpio::power_off_amp()?
                match gpio::power_off_amp(){
                    Ok(_)=> (),
                    Err(_)=> ()
                }
            }
            _ => (),
        }
    }
    Ok(())
}
// async fn connect_to_device(device_address: &str) {
//     // Create a new manager instance
//     let manager = Manager::new().await.unwrap();

//     let central = get_central(&manager).await;

//     let address = PeripheralId::try_from("fo");
//     // Look for the device with the specified address
//     let peripheral = central.peripheral(device_address).unwrap();

//     // Connect to the device
//     match peripheral.connect() {
//         Ok(_) => {
//             println!("Connected to device with address '{}'.", device_address);
//             // Perform further operations with the connected device
//         }
//         Err(err) => {
//             eprintln!("Failed to connect to device: {:?}", err);
//             return;
//         }
//     }

//     // Disconnect from the device when done
//     peripheral.disconnect().unwrap();
// }
