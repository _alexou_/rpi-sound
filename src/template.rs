use std::collections::HashMap;

use lazy_static::lazy_static;
// use serde_json::value::{to_value, Value};
use tera::{Context, Tera};

use crate::config;

lazy_static! {
    static ref TEMPLATES: Tera = {
        let mut tera = match Tera::new("views/*") {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                ::std::process::exit(1);
            }
        };

        tera
    };
}
pub fn render_preferences() -> String {
    let mut context = Context::new();
    // behaviours for the button
    let mut button = HashMap::new();
    button.insert("option1", config::ButtonBehaviour::Connect);
    button.insert("option2", config::ButtonBehaviour::DisconnectBt);
    button.insert("option3", config::ButtonBehaviour::None);
    button.insert("option4", config::ButtonBehaviour::ShutDown);
    context.insert("button_behaviour", &button);

    // behaviour when connecting the phone
    let mut connect = HashMap::new();
    connect.insert("option1", config::BluetoothBehaviour::PowerSwitch);
    connect.insert("option2", config::BluetoothBehaviour::None);
    context.insert("connect_behaviour", &connect);

    let current_config = config::get_config();



    TEMPLATES
        .render("preferences.html", &context)
        .expect("Failed to render template")
}